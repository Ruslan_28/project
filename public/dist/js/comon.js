$(document).ready(function(){ 

    //scroll header animate
    const header = $(".headerBox");
    var lastScrollTop = 0;
    $(window).scroll(function(event){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
        if(!header.hasClass('transformHeader')){
            header.addClass('transformHeader');
        }
    } else {
        header.removeClass('transformHeader');
    }
    lastScrollTop = st;
    });


    //accordion
    function accordion(el){
        el.find(".title").click(function(){
            $(this).next(".content").slideToggle('fast');
            $(".content").not($(this).next(".content")).slideUp('fast');
        });
    }
    $(document).ready(function(){
        accordion($(".accordion"));
    });


    //add file input
    $('#chooseFile').bind('change', function () {
        var filename = $("#chooseFile").val();
        if (/^\s*$/.test(filename)) {
          $(".file-upload").removeClass('active');
          $("#noFile").text("No file chosen..."); 
        }
        else {
          $(".file-upload").addClass('active');
          $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
        }
    });
    

    //add iframe video popUp
    $('.playImg').on('click', function(e) {
        e.preventDefault()
        const scriptContent = $(this).find('script')[0].innerHTML
        const iframeString = JSON.parse(scriptContent).items[0].html
        $('body').append( `<div class="poopUpBox"> <img class="imgClose" src="dist/img/close.svg" alt=""> ${iframeString} <> </div>` )
        $('body').addClass('addOverflove')

        $(".imgClose").on('click', function() {
            $(".poopUpBox").remove()
            $('body').removeClass('addOverflove')
        })
    })

    //close popup click body
    $(document).mouseup(function (e) {
        var container = $(".poopUpBox");
        if (container.has(e.target).length === 0){
            container.remove();
            $('body').removeClass('addOverflove')
        }
    });

})

